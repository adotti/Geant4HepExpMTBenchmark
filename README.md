Bootstrap script for HepExpMT benchamrk
=======================================

This is a simple script that allows to download, configure and compile
the HepExpMT application benchmark. 
Please refer to the project [twiki](https://twiki.cern.ch/twiki/bin/view/Geant4/Geant4HepExpMTBenchmark)
for instructions and information.


Prerequisite
------------
Bash shell on Linux. 

Installation
------------
 1. Clone this package 
 2. cd into the new directory
 3. ``chmod +x bootstrap.sh``
 4. ``./bootstrap.sh -h`` for instructions
 
