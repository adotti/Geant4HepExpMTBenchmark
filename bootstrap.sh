#!/bin/bash

#Configuration
localtarballfilename="G4test.tar.gz"
# cpsrc==-1: code already in place, cpsrc==0 download, cpsrc==1, copy from local tarballs
cpsrc=-1
gbasedir=$(dirname $0)
basedir=$(cd $gbasedir > /dev/null ; pwd )
#CXX=g++
#CC=gcc
COMPILER=c++
CCOMPILER=cc
TCXXFLAGS=""
SHARED=0
TYPE=Release
#TYPE=RelWithDebInfo
defaultodir=1
OUTPUTDIR=`uname -m`-`uname`
bcm=0
bxc=0
bg4=1
pkgs=2
extracmake=""
parj=1
grep "processor" /proc/cpuinfo &> /dev/null 2>&1
if [ $? -eq 0 ];then
    parj=`grep "processor" /proc/cpuinfo | tail -n1 | awk '{print $3};'`
fi
steps=2
mic=0
verbose=0
unpack=1

usage()
{
cat << EOF
usage $0 [-vhstocfixmDjpg] [default]
if "$0 default" is used, use default configuration, download and compile code. Add option to overwrite defaults.
if "$0 -o <somedir>" is used, assume all code is already prepared in <somedir>, just compile.
options:
  -h : show this help
  -v : turn on verbosity in this script
  -s : build shared libraries (default STATIC)
  -t type : set value of CMAKE_BUILD_TYPE option (default Release)
  -o prefix : output directory for final directory (default guessed from system)
  -c [gcc|icc|clang|other] : compiler family (default gcc). If "other", use CXX and CC environment variables as c++ and c compilers.
  -f flags : extra compiler flags
  -i tarball : use specified tarball(s) instead of automatic download
  -x : build Xerces-C from srcdir/xerces* (default OFF)
  -m : build CMake from srcdir/cmake* (default OFF)
  -D : extra CMake options (can be used more than once) for G4 compilation
  -j : number of parallel jobs for compilation
  -g : G4 is already built and in the output directory
  -p <toolchainfile> : compile for Intel Xeon Phi architecture given the provided CMake toolchian file.
                       Note, environmnet variables CXX, CC, AR and LD *must* be set to proper tools.
                       Example:
                       export CXX="icpc"
                       export CC="icc"
                       export AR=/usr/linux-k1om-4.7/bin/x86_64-k1om-linux-ar
                       export LD=/usr/linux-k1om-4.7/bin/x86_64-k1om-linux-ld
Examples:
   Rely on script for everything:
      ./bootstrap.sh default
   Ask script to build also cmake and Xerces-C:
      ./bootstrap.sh -x -m default
   Provide to the script custom tarballs (e.g. provide custom G4 version)
      ./bootstrap.sh -i G4app-src-Current.tar.gz -i geant4-XX-YY-ZZ.tar.gz default
   Compile content of final directory with system cmake and user installation of Xerces-C:
      ./bootstrap.sh -o testdir -DXERCESC_ROOT_DIR=/some/dir  
EOF
}

download()
{
    website="https://cernbox.cern.ch/index.php/s/3MNrtycrirGalYa/download"
    cmdline="curl -# -0 ${website} -o ${localtarballfilename}"
    $cmdline
}

parseme()
{
    while getopts "hst:o:c:f:i:xmgj:D:p:v" OPTION
    do
	case $OPTION in
            D)
		extracmake="${extracmake} -D${OPTARG}"
		;;
            h)
		usage
		exit 1
		;;
            t)
		TYPE=$OPTARG
		;;
            s)
		SHARED=1
		;;
            o)
		OUTPUTDIR=$OPTARG
		defaultodir=0
		;;
            c)
		if [ $OPTARG == "gcc" ];then
                    export CXX=g++
                    export CC=gcc
		elif [ $OPTARG == "icc" ];then
                    COMPILER=icc
                    export CXX=icpc
                    export CC=icc
		elif [ $OPTARG == "clang" ];then
                    COMPILER=clang
                    export CXX=clang++
                    export CC=clang
		elif [ $OPTARG == "other" ];then
		    COMPILER=$CXX
		    decho "Setting custom compier, rely on env variables"
		    if [ "x$CXX" == "x" ];then
			echo "Set CXX environment variable to C++ compiler"
			exit 1
		    fi
		    if [ "x$CC" == "x" ];then
			echo "Set CC environment variable to C compiler"
			exit 1
		    fi
		else
		    echo "Option not recognized"
		    usage
		    exit 1
		fi
		;;
            i)
		cpsrc=1
		src="${src} $OPTARG"
		;;
            f)
		TCXXFLAGS=$OPTARG
		;;
            x)
		bxc=1
		steps=$(( $steps + 1 ))
		;;
            m)
		bcm=1
		steps=$(( $steps + 1 ))
		;;
            g) 
	 	bg4=0
		steps=$(( $steps - 1 ))
		;;
            j)
		parj=$OPTARG
		;;
	    p)
		mic=1
		toolchainfile=$OPTARG
		;;
            v) 
		verbose=1
		;;
	esac
    done
  if [ "x"$TCXXFLAGS != "x" ];then
      export CXXFLAGS="${CXXFLAGS} $TCXXFLAGS "
      export CFLAGS="${CFLAGS} $TCXXFLAGS "
  fi
}

decho()
{
   [ $verbose -gt 0 ] && echo $1
}

buildinstareapath()
{
    decho "Using $CXX --version go guess compiler version"
    $CXX --version &> /dev/null
    if [ $? -eq 0 ];then
	cver=`$CXX --version 2>&1 | cut -d" " -f3 | head -n1 | sed s/"\."//g `
	re='^[0-9]+$'
	if ! [[ $cver =~ $re ]] ; then
	    cver=`$CXX --version 2>&1 | cut -d" " -f4 | head -n1 | sed s/"\."//g `
	    if ! [[ $cver =~ $re ]] ; then
		cver="UNKNOWNVERSION"
	    fi
	fi
    else
	echo "Compiler version unknown"
	cver="UNKNOWN"
    fi
    if [ $defaultodir -eq 1 ];then
	OUTPUTDIR=${OUTPUTDIR}-${COMPILER}-${cver}-${TYPE}
	[ $SHARED -eq 0 ] && OUTPUTDIR=${OUTPUTDIR}-static
    fi
    instarea=${basedir}/${OUTPUTDIR}
    decho "Installation area is $instarea"
}

dumpinfo()
{
    decho "===Configuration:==="
    decho "Compiler family: "$COMPILER
    decho "Shared LIB Option: "$SHARED
    decho "Build Type: "$TYPE
    decho "Installation Directory: "$instarea
    if [ "x"$TCXXFLAGS != "x" ];then
	decho "extra CXX/C Flags: ->"$CXXFLAGS"<->"$CFLAGS"<-"
    fi
    decho "Compilation logfile: $PWD/compilation.log"
    decho "Build cmake:"$bcm
    decho "Build Xerces-C:"$bxc
    decho "======="
}

unpack()
{
    echo "=============Unpacking =============" >> compilation.log
    for fn in ${localtarballfilename};do
	ls -lrt ../${fn} >> compilation.log
	which md5 > /dev/null 2>&1 
	[ $? -eq 0 ] && md5 ../${fn} >> compilation.log
	decho "tar xzf ../${fn}"
	tar xzf ../${fn}
    done
}

checkproduct()
{
    ls ${instarea}/$1* > /dev/null
    if [ $? -ne 0 ];then
	echo "ERROR: Cannot compile $2, check compilation.log file"
	exit 1
    fi

}

compilecmake()
{
    cd cmake*
    decho "Compiling cmake in $PWD"
    decho "./bootstrap --prefix=$instarea"
    echo "================ Compiling CMake ===============" >> ../compilation.log
    ./bootstrap --prefix=$instarea >> ../compilation.log 2>&1
    decho "make -j ${parj}"
    make -j ${parj} >> ../compilation.log 2>&1
    decho "make install"
    make install >> ../compilation.log 2>&1
    ls ${instarea}/bin/cmake > /dev/null
    if [ $? -ne 0 ];then
	echo "ERROR: Cannot compile XercesC, check compilation.log file"
	exit 1
    fi
    cmakebin=${instarea}/bin/cmake
    checkproduct bin/cmake "CMake"
    cd ..
}

compilexercesc()
{
    cd xerces*
    decho "Compiling xercesc in $PWD"
    xopts=" --prefix=${instarea} "
    #Needed for KNL
    xopts=" --disable-transcoder-icu "$xopts
    if [ $mic -eq 1 ];then
	decho "Cross-compilation for MIC architecture"
       oldLDF=$LDFLAGS
       oldCXX=$CXXFLAGS
       oldCF=$CFLAGS
       export LDFLAGS="$LDFLAGS -mmic "
       export CXXFLAGS="$CXXFLAGS -w -O2 -DNDEBUG -mmic "
       export CFLAGS="$CFLAGS -w -O2 -DNDEBUG -mmic "
       xopts="${xopts} --host=x86_64-intel-linux CC=icc CXX=icpc "
    fi
    [ $SHARED -eq 0 ] && xopts=" --disable-shared --disable-netaccessor-curl "$xopts
    #[ "x"$MACOSX != "x" ] && xopts=" --disable-transcoder-macosunicodeconverter "$xopts
    decho "./configure ${xopts}"
    echo "================ Compiling Xerces-C ===============" >> ../compilation.log
    ./configure ${xopts} >> ../compilation.log 2>&1
    decho "make clean"
    make clean >> ../compilation.log 2>&1
    decho "make -j ${parj}"
    if [ $verbose == 1 ];then
	make -j ${parj} VERBOSE=1 >> ../compilation.log 2>&1
    else
	make -j ${parj}  >> ../compilation.log 2>&1
    fi
    decho "make install"
    make install >> ../compilation.log 2>&1
    checkproduct lib/libxerces-c "Xerces-C"
    cd ..
    if [ $mic -eq 1 ];then
	export LDFLAGS=$oldLDF
	export CXXFLAGS=$oldCXX
        export CFLAGS=$oldCF
    fi
}

buildApp()
{
  builddir=$1
  appname=$2
  srcdir=$3
  output=$4
  mkdir -p ${builddir}
  cd ${builddir}
  decho "Compiling ${appname} application in $PWD"
  decho "Geant4 from: "${instarea}
  if [ -d ${instarea}/lib64 ];then
      xopts=" -DCMAKE_INSTALL_PREFIX=${instarea} -DGeant4_DIR=${instarea}/lib64/Geant4-* -DCMAKE_BUILD_TYPE=$TYPE "
  else
      xopts=" -DCMAKE_INSTALL_PREFIX=${instarea} -DGeant4_DIR=${instarea}/lib/Geant4-* -DCMAKE_BUILD_TYPE=$TYPE "
  fi
  if [ $SHARED -eq 0 ];then
      xopts="${xopts} -DBUILD_SHARED_LIBS=OFF -DBUILD_STATIC_LIBS=ON "
  else
      xopts="${xopts} -DBUILD_SHARED_LIBS=ON -DBUILD_STATIC_LIBS=OFF "
  fi
  if [ "x"$TCXXFLAGS != "x" ];then
      xopts="${xopts} -DCMAKE_CXX_FLAGS=\"$CXXFLAGS\" -DCMAKE_C_FLAGS=\"$CFLAGS\" "
  fi
  xopts="${xopts} -DCMAKE_CXX_COMPILER=$CXX -DCMAKE_C_COMPILER=$CC "
  if [ $mic -eq 1 ];then
      decho "Cross-compilation for MIC architecture"
      oldLDF=$LDFLAGS
      oldCXX=$CXXFLAGS
      oldCF=$CFLAGS
      export LDFLAGS="$LDFLAGS -mmic "
       export CXXFLAGS="$CXXFLAGS -w -O2 -DNDEBUG -mmic "
      export CFLAGS="$CFLAGS -w -O2 -DNDEBUG -mmic "
      xopts="${xopts} -DCMAKE_TOOLCHAIN_FILE=${toolchainfile} -DCMAKE_AR=${AR} -DCMAKE_LINKER=${LD}"
  fi
  decho "$cmakebin ${xopts} ${srcdir}"
  echo "================ Compiling ${appname} ===============" >> ${instarea}/compilation.log
  $cmakebin ${xopts} ${srcdir} >> ${instarea}/compilation.log 2>&1
  decho "make -j ${parj} VERBOSE=1 "
  make -j ${parj} VERBOSE=1 >> ${instarea}/compilation.log 2>&1
  decho "make install"
  make install >> ${instarea}/compilation.log 2>&1
  checkproduct ${output} "${appname} Application"
  cd ..
  if [ $mic -eq 1 ];then
      export LDFLAGS=$oldLDF
      export CXXFLAGS=$oldCXX
      export CFLAGS=$oldCF
  fi
 
}

compileG4()
{
    mkdir -p build-G4
    cd build-G4
    decho "Compinging G4 in $PWD"
    #Adding default cmake arguments if user did not specify
    xopts="${extracmake} -DGEANT4_USE_GDML=ON -DGEANT4_USE_SYSTEM_EXPAT=OFF -DCMAKE_BUILD_TYPE=$TYPE "
    [[ ! $xopts == *GEANT4_BUILD_MULTITHREADED* ]] && xopts="${xopts} -DGEANT4_BUILD_MULTITHREADED=ON "
    [[ ! $xopts == *GEANT4_INSTALL_DATA* ]] &&  xopts="${xopts} -DGEANT4_INSTALL_DATA=ON "
    [[ ! $xopts == *CMAKE_INSTALL_PREFIX* ]] && xopts="${xopts} -DCMAKE_INSTALL_PREFIX=${instarea} "
    [[ ! $xopts == *XERCESC* ]] && xopts="${xopts} -DXERCESC_ROOT_DIR=${instarea} "
    #xopts=" -DGEANT4_BUILD_MULTITHREADED=ON -DCMAKE_INSTALL_PREFIX=${instarea} "
    #xopts="${xopts} -DGEANT4_USE_GDML=ON -DXERCESC_ROOT_DIR=${instarea}"
    #xopts="${xopts} -DGEANT4_USE_SYSTEM_EXPAT=OFF -DCMAKE_BUILD_TYPE=$TYPE "
    if [ $SHARED -eq 0 ];then
	xopts="${xopts} -DBUILD_SHARED_LIBS=OFF -DBUILD_STATIC_LIBS=ON "
    else
	xopts="${xopts} -DBUILD_SHARED_LIBS=ON -DBUILD_STATIC_LIBS=OFF "
    fi
    if [ "x"$TCXXFLAGS != "x" ];then
	xopts="${xopts} -DCMAKE_CXX_FLAGS=\"$CXXFLAGS\" -DCMAKE_C_FLAGS=\"$CFLAGS\" "
    fi
    [ "x"$CXX != "x" ] && xopts="${xopts} -DCMAKE_CXX_COMPILER=$CXX "
    [ "x"$CC != "x" ] &&  xopts="${xopts} -DCMAKE_C_COMPILER=$CC "
    #xopts="${xopts} -DCMAKE_CXX_COMPILER=$CXX -DCMAKE_C_COMPILER=$CC "
    #if [ "x$extracmake" != "x" ];then
#	xopts="${xopts} ${extracmake} "
 #   fi

    #Specific for cross-compiling on MIC
    if [ $mic -eq 1 ];then
       decho "Cross-compilation for MIC architecture"
       oldLDF=$LDFLAGS
       oldCXX=$CXXFLAGS
       oldCF=$CFLAGS
       export LDFLAGS="$LDFLAGS -mmic "
       export CXXFLAGS="$CXXFLAGS -w -O2 -DNDEBUG -mmic "
       export CFLAGS="$CFLAGS -w -O2 -DNDEBUG -mmic "
       xopts="${xopts} -DCMAKE_TOOLCHAIN_FILE=${toolchainfile} -DCMAKE_AR=${AR} -DCMAKE_LINKER=${LD} "
       xopts="${xopts} -DCMAKE_CXX_FLAGS_RELEASE=-mmic -DCMAKE_C_FLAGS_RELEASE=-mmic "
       xopts="${xopts} -DCMAKE_CXX_FLAGS_RELWITHDEBINFO=-mmic -DCMAKE_C_FLAGS_RELWITHDEBINFO=-mmic "
       xopts="${xopts} -DCMAKE_CXX_FLAGS_DEBUG=-mmic  -DCMAKE_C_FLAGS_DEBUG=-mmic "
       if [[ ! ( $xopts == *XERCESC_INCLUDE_DIR* && $xopts == *XERCESC_LIBRARY* ) ]];then
	   echo "LIMITATION: sorry, specify -DXERCESC_INCLUDE_DIR and -DXERCESC_LIBRARY explictly"
	   exit 1
       fi
       #For a strange reason if useing cross-compilation we need to specify these two explicitly...
       #if [ $SHARED -eq 0 ];then
       #   xopts="${xopts} -DXERCESC_INCLUDE_DIR=${instarea}/include -DXERCESC_LIBRARY=${instarea}/lib/libxerces-c.a "
       #else
       #   xopts="${xopts} -DXERCESC_INCLUDE_DIR=${instarea}/include -DXERCESC_LIBRARY=${instarea}/lib/libxerces-c.so "
       #fi
    fi
    decho "$cmakebin ${xopts} ../geant4*"
    echo "================ Compiling Geant4 ===============" >> ../compilation.log
    $cmakebin ${xopts} ../geant4* >> ../compilation.log 2>&1
    decho "make -j ${parj} VERBOSE=1 "
    make -j ${parj} VERBOSE=1 >> ../compilation.log 2>&1
    decho "make install"
    make install >> ../compilation.log 2>&1
    buildApp buildB1 exampleB1 `find ${instarea}/geant4*/examples -type d -name B1` bin/exampleB1
    cd ..
    if [ $mic -eq 1 ];then
	export LDFLAGS=$oldLDF
	export CXXFLAGS=$oldCXX
	export CFLAGS=$oldCF
    fi
}


compileHepExpMT()
{
    buildApp build-HepExpMT HepExpMT ${instarea}/HepExpMT bin/HepExpMT
}

step=0
dostep()
{
    step=$(( $step + 1 ))
    echo "[$step/$steps] $1..."
    $2
}


dotest()
{
    ./run.sh 1 1 2>&1 >> compilation.log
    if [ $? -ne 0 ];then
	echo "ERROR: cannot run application check log file"
    fi
}

checkcmd()
{
    which $1 &> /dev/null
    if [ $? -ne 0 ];then
	echo "ERROR: Cannot find $1 in $PATH"
	exit 1
    fi
}

#### MAIN ##########
if [ $# -lt 1 ];then
   usage
   exit 1
fi
if [ $# -ge 1 ];then
    aa=${@: -1}
    if [ "$aa" == "default" ];then 
       decho "Defaults "
       cpsrc=0
    fi
fi
echo "Full log in $PWD/<installationarea>/compilation.log"

parseme "$@"
#If download of tarball or not-MIC two additional steps
[ $cpsrc -eq 0 ] && steps=$(( $steps + 1 ))
[ $cpsrc -ge 0 ] && steps=$(( $steps + 1 ))
#[ $mic -eq 0 ] && steps=$(( $steps + 1 ))
#If no compiler was specified, set it now:
[ "x"$CXX == "x" ] && export CXX=$COMPILER
[ "x"$CC == "x" ] && export CC=$CCOMPILER
buildinstareapath
dumpinfo
step=0
if [ $bcm == 0 ];then
    cmakebin=cmake
    checkcmd cmake
fi
checkcmd $CXX
if [ ! -d ${instarea} ];then
    decho "creating installation directory"
    mkdir -p ${instarea}
else
    decho "Installation directory already exists"
fi
if [ $cpsrc -eq 0 ]; then
    dostep "Downloading tarball" download 
elif [ $cpsrc -eq 1 ];then
    localtarballfilename="$src"
    decho "Local copy of code in: "${localtarballfilename}
fi
cd $instarea
touch compilation.log
echo "==============================================" >> compilation.log
echo "==============================================" >> compilation.log
echo "Starting bootstrap.sh on: "`hostname` >> compilation.log
echo "On: "`date` >> compilation.log
echo "Options: $@" >> compilation.log 
echo "Installation dir: ${instarea}" >> compilation.log
echo "==============================================" >> compilation.log
echo "==============================================" >> compilation.log

[ $cpsrc -ge 0 ] && dostep "Unpacking" unpack
#Start compilation of packages
#CMake
if [ $bcm -eq 1 ];then
    dostep "Compiling cmake" compilecmake
fi
decho "Using cmake binary: $cmakebin"
#Xerces-C
[ $bxc -eq 1 ] && dostep "Compiling Xerces-C" compilexercesc
#Geant4
[ $bg4 -eq 1 ] && dostep "Compiling Geant4" compileG4
#Application
dostep "Compiling HepExpMT" compileHepExpMT
#[[ $mic -eq 0 && -e run.sh ]] && dostep "Testing application" dotest
echo "====== All Done, content of build directory: =======" >> compilation.log
ls ${instarea} >> compilation.log
echo "==== bin: ">>compilation.log
ls ${instarea}/bin >> compilation.log
echo "==== lib*: ">>compilation.log
ls ${instarea}/lib* >> compilation.log
echo "==== Bye: "`date` >> compilation.log
echo "==============================================="  >> compilation.log
echo "Done!"
